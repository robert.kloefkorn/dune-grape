// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef GRAPE_GRAPEHMESH_HH_INCLUDED
#define GRAPE_GRAPEHMESH_HH_INCLUDED

#if HAVE_GRAPE

#ifndef GRAPE_DIM
#define GRAPE_DIM 2
#endif

#ifndef GRAPE_DIMWORLD
#define GRAPE_DIMWORLD 2
#endif

#undef GRAPE_ELDESC_HH
#undef GRAPE_HMESH_HH
#undef GRAPE_HMESH_INLINE_HH

#include "ghmesh_inline.hh"

#if GRAPE_DIM == 3
#define G_CPP
#undef GRAPE_PARTITIONDISPLAY_HH
#include "partitiondisplay.hh"
#undef G_CPP
#endif

#undef GRAPE_DIM
#undef GRAPE_DIMWORLD

#undef HMesh
#undef GenMesh
#undef GrapeMesh
#endif

#endif
